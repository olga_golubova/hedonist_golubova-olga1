const assert = require('assert');

class commonFunc
{

    waitForSpinner(){
        const spinner = $('div#preloader');
        spinner.waitForDisplayed(20000);
        spinner.waitForDisplayed(10000, true);
    }

    waitForNotification(){
    const notification = $('div.toast div');
    notification.waitForDisplayed(10000, true);
    } 

    getTestResult(msgText)
    {
    const notification = $('div.toast div');
    return assert.strictEqual(notification.getText(), msgText);
    }

    checkLinkChange(linkUrl, currentLinkUrl)
    {
        return assert.strictEqual(linkUrl, currentLinkUrl);
    }
}

module.exports = new commonFunc();