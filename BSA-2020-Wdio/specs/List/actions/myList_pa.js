const myListPage = require('../pages/myList_po.js');
const page = new myListPage();

class myListPageActions
{
    
    addNewList()
    {
        page.addNewListButton.waitForDisplayed(2000);
        page.addNewListButton.click();
    }

    deleteFisrtList()
    {
        page.deleteListButton.waitForDisplayed(2000);
        page.deleteListButton.click();        
    }

    confirmDelete()
    {
        page.modalWindow.waitForDisplayed(2000);
        page.modalDeleteButton.waitForDisplayed(2000);
        page.modalDeleteButton.click();
    }
}

module.exports = myListPageActions;
