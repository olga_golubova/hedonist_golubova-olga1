const LoginPage = require('../page/LoginSettings_po');
const page = new LoginPage();

class LoginActions
{
    
    makeLogin(email, password)
    {
 
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(email);

        page.newPasswordInput.waitForDisplayed(2000);
        page.newPasswordInput.setValue(password);

        page.createButton.waitForDisplayed(2000);
        page.createButton.click();
    }
 
}

module.exports = LoginActions;
