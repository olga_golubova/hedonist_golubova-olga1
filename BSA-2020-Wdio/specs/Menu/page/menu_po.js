class MenuProfil{
    get menuProfileDropDown() {return $('div.profile')}
    get mylistOption() {return $('a[href="/my-lists"]')}

    get signInMenu() {return $('a[href="/signup"]')}
}

module.exports = MenuProfil; 